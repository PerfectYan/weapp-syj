//index.js

//获取应用实例
var app = getApp();

Page({
    data: {
        hidden:true,
        cate_id: '',
        limit: 8,
        scrollTop: 0,
        loadingMoreHidden: true,
        goodsList: []
    },
    onLoad(options) {
      if (options.cate_id) {
        this.setData({
          cate_id: options.cate_id
        });
        this.getGoodsList();
      }
      else {

        app.Toast.showLoading()
        var goodsList = wx.getStorageSync('productList');
        this.setData({ goodsList, hidden: goodsList.length > 0 ? true : false  });
        setTimeout(() => { app.Toast.hideLoading()  },1000)


       
      }

    },
    onShow() {
    },
    getGoodsList() {
        let params = {
            cate_id: this.data.cate_id,
            page: 1,
            limit: this.data.limit
        };
        app.api.getGoodsList(params).then(res => {
            let goodsList = res.content;
            this.setData({ goodsList, hidden: goodsList.length>0?true:false});
        })
    },
    addToCart(e) {
        let id = e.currentTarget.dataset.id;
        let gspec_id = e.currentTarget.dataset.id;
        let params = {
            userId: app.globalData.userId,
            goods_id: id,
            gspec_id: gspec_id,
            goods_number: 1
        };
        app.api.addToCart(params).then(function () {
            app.Toast.success("加入购物车成功");
        })
    },
    orderDetailsTap(e) {
        wx.navigateTo({
            url: "/pages/goods-details/index?id=" + e.currentTarget.dataset.id
        })
    },
    onPageScroll(e) {
        let scrollTop = this.data.scrollTop;
        // console.log(scrollTop);
        this.setData({
            scrollTop: e.scrollTop
        })
    },
    onReachBottom() {
        this.data.limit += 8;
        this.getGoodsList();
    },
    onShareAppMessage() {
        return {
            title: app.globalData.shareProfile,
            path: '/pages/index/index',
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    },
});
