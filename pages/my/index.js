const app = getApp();

Page({
    data: {
        userId: '',
        orderTypes: [
            {
                icon: 'https://www.ishiyaji.com/static/images/my/icon-wallet.png',
                name: '待付款',
                type: 2,
                orderType: 'wzf'
            }, {
                icon: 'https://www.ishiyaji.com/static/images/my/icon-truck.png',
                name: '待发货',
                type: 3,
                orderType: 'dfh'
            }, {
                icon: 'https://www.ishiyaji.com/static/images/my/icon-package.png',
                name: '待收货',
                type: 4,
                orderType: 'dsh'
            },
            {
                icon: 'https://www.ishiyaji.com/static/images/my/icon-order.png',
                name: '全部订单',
                type: 1,
                orderType: 'all'
            }, {
                icon: 'https://www.ishiyaji.com/static/images/my/icon-refund.png',
                name: '售后订单',
                type: 5,
                orderType: 'refund'
            }
        ],
        balance: 0,
        freeze: 0,
        score: 0,
        score_sign_continuous: 0,
        account_id: '',
        level: '',
        level_name: '',
        zmjf: {"jrzm": 0, "byzm": "0", "zzm": "0"},
        sxjf: {"jrxs": 0, "byxs": "0", "zxs": "0"},
        myteam: {"coachDz": 0, "adminDz": 0},
        myxs: {"jrxse": 0, "byxse": 0, "zxse": 0},
        myzj: {
            "byxse": 0,
            "byxseCha": 0,
            "xseprveCha": "+0",
            "num": 0,
            "numCha": 0,
            "numprveCha": "+0",
            "next_level": ''
        },
        all: 0,
        ordernum: {
            wzf: 0,
            dfh: 0,
            dsh: 0,
            refund: 0
        }
    },
    onLoad(options) {
        // console.log('my-options = ',options);
        if (options.invite_code) {
            wx.setStorageSync('invite_code', options.invite_code);
        }
    },

    initLogin(o) {
        let userId = o.detail;
        let self = this;
        wx.setStorageSync('_userId', userId);
        wx.getStorage({
            key: 'level',
            success: function (res) {
                self.setData({
                    level: res.data
                });
            }
        });
        wx.getStorage({
            key: 'account_code',
            success: function (res) {
                self.setData({
                    account_code: res.data
                });
            }
        });

        app.api.getUserInfoCenter({userId}).then(res => {
            let data = res.content;
            let ordernum = data.ordernum;
            ordernum.all = this.getAllNum(data.ordernum);
            this.setData({
                zmjf: data.zmjf,
                sxjf: data.sxjf,
                myteam: data.myteam,
                myxs: data.myxs,
                myzj: data.myzj,
                ordernum
            })
        });
        app.api.getPersonerInfo({userId}).then(res => {
            let data = res.content || {};
            this.setData({
                avatar: data.avatar,
                nickname: data.nickname,
                level: data.level,
                level_name: data.level_name,
                account_id: data.account_id,
                parent_name: data.parent_name
            });
            wx.setStorage({
                key: 'level',
                data: data.level
            });
        })
    },
    onShow() {
        let userId = wx.getStorageSync('_userId');
        userId && this.initLogin({ detail: userId})
    },
    getAllNum(ordernum) {
        let n = 0;
        for (var key in ordernum) {
            n += ordernum[key]
        }
        return n
    },
    statusTap: function (e) {
        var type = e.currentTarget.dataset.type;
        var url = "/pages/my-order-list/index?type=" + type;
        // console.log(url);
        wx.navigateTo({
            url: url
        })
    },
    zmjfTap: function () {
        wx.navigateTo({
            url: '/pages/points-recruitment/index'
        })
    },
    xsjfTap: function () {
        wx.navigateTo({
            url: '/pages/points-sales/index'
        })
    },
    myTeamTap: function () {
        wx.navigateTo({
            url: '/pages/my-team/index'
        })
    },
    pannelTap: function (e) {
        var url = e.currentTarget.dataset.url;
        wx.navigateTo({
            url: url
        })
    },
    serviceTap: function (e) {
        var url = e.currentTarget.dataset.url;
        wx.navigateTo({
            url: url
        })
    },
    onShareAppMessage() {
        let level = this.data.level;
        let account_code = this.data.account_code;
        let url = '';
        if (level >= 1) {
            url = '/pages/my/index?invite_code=' + account_code;
        } else {
            url = '/pages/my/index';
        }
        // console.log('my_share_url = ',url);
        return {
            title: app.globalData.shareProfile,
            path: url,
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    }
});