import strategys from '../../utils/strategy.js'
import Validator from '../../utils/validator.js'

var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        bankList: [],
        disabled: true,
        selectedBankIndex: 0,
        selectedBank: '',
        index: '',
        region: ['省份', '城市', '区/县'],
        in_city: '',
        customItem: '请选择',
        real_name: '',
        phone: "",
        wechat: '',
        idcard: '',
        bank_id: 1,
        bank_person: '',
        bank_account: '',
        bank_branch: ''
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let userId = app.globalData.userId;
        app.api.getBankList({
            userId
        }).then(res => {
            let data = res.content;
            data.unshift({
                bank_id: '',
                bank_name: '请选择收款银行'
            });
            this.setData({
                bankList: data
            });
            setTimeout(() => {
                this.getPersonerInfo(userId)
            }, 100)

        }).then(res => {
            // this.getPersonerInfo(userId)
        })
    },
    getPersonerInfo(userId) {
        app.api.getPersonerInfo({
            userId
        }).then(res => {
            let data = res.content;
            let selectedBankIndex = this.data.bankList.findIndex((value) => {
                return value.bank_id == data.bank_id
            });
            if(data.in_city == ''){
                this.setData({
                    region: ['请选择', '请选择', '请选择']
                });
            }else{
                this.setData({
                    region: data.in_city.split(",")
                });
            }
            this.setData({
                real_name: data.real_name,
                phone: data.phone,
                wechat: data.wechat,
                idcard: data.idcard,
                selectedBankIndex, // 银行
                bank_person: data.bank_person,
                bank_account: data.bank_account,
                bank_branch: data.bank_branch
            })
        })
    },
    bindBankChange: function (e) {
        let index = e.detail.value;
        let bank_id = this.data.bankList[index].bank_id;

        this.setData({
            bank_id,
            selectedBankIndex: index,
        });

    },
    bindRegionChange: function (e) {
        // console.log('picker发送选择改变，携带值为', e.detail.value);
        // JSON.stringify(e.detail.value)
        this.setData({
            region: e.detail.value,
            in_city: e.detail.value.toString()
        })
    },
    formSubmit(e) {
        // console.log('form发生了submit事件，携带数据为：', e.detail.value);
        var errorMsg = this.validateFunc(e.detail.value);
        if (errorMsg) {
            app.Toast.warn(errorMsg);
            return
        }
        let params = e.detail.value;
        // console.log(params);
        params["userId"] = app.globalData.userId;
        params["bank_id"] = this.data.bank_id;
        params["in_city"] = this.data.in_city;

        app.api.editPersonerInfo(params).then(res => {
            app.Toast.success('保存成功');
            wx.navigateBack({
                delta: 1,
            })
        })

    },
    validateFunc: function (detail) {
        var validator = new Validator(strategys); // 创建一个Validator对象

        /* 添加一些效验规则 */

        validator.addRules(detail.real_name, [{'strategy': 'isNotEmpty', 'errorMsg': '请输入姓名'}],
            function () {
                // console.log('验证通过');
            }
        );
        validator.addRules(detail.phone, [{'strategy': 'mobileFormat', 'errorMsg': '请输入正确的手机号'}],
            function () {
                // console.log('验证通过');
            }
        );

        validator.addRules(detail.wechat, [{'strategy': 'isNotEmpty', 'errorMsg': '请输入微信号'}],
            function () {
                // console.log('验证通过');
            }
        );

        validator.addRules(detail.idcard, [{'strategy': 'idCard', 'errorMsg': '请输入正确的身份证号'}],
            function () {
                // console.log('验证通过');
            }
        );

        validator.addRules(detail.address.toString(), [{ 'strategy': 'inCity', 'errorMsg': '请选择完整的城市地址' }],
          function () {
            // console.log('验证通过');
          }
        );

        validator.addRules(detail.bank_id, [{'strategy': 'isNotEmpty', 'errorMsg': '请选择收款银行'}],
            function () {
                // console.log('验证通过');
            }
        );

        if (detail.bank_person != '') {
            validator.addRules(detail.bank_person, [{'strategy': 'isChinese', 'errorMsg': '银行账户只能包含中文'}],
                function () {
                    // console.log('验证通过');
                }
            );
        }

        if (detail.bank_account != '') {
            validator.addRules(detail.bank_account, [{'strategy': 'bankCard', 'errorMsg': '请输入不少于13位数的银行账号'}],
                function () {
                    // console.log('验证通过');
                }
            );

        }

        if (detail.bank_branch != '') {
            validator.addRules(detail.bank_branch, [{'strategy': 'isChinese', 'errorMsg': '银行分行只能包含中文'}],
                function () {
                    // console.log('验证通过');
                }
            );

        }


        var errorMsg = validator.start(); // 执行验证函数，并返回效验结果
        return errorMsg;

    }
});