//获取应用实例
var app = getApp();
Page({
    data: {
        addressList: []
    },
    addAddress: function () {
        wx.navigateTo({
            url: "/pages/address-add/index"
        })
    },
    editAddress: function (e) {
        wx.navigateTo({
            url: "/pages/address-add/index?id=" + e.currentTarget.dataset.id
        })
    },
    deleteAddress: function (e) {
        let self = this;
        wx.showModal({
            title: '提示',
            content: '确定要删除该收货地址吗？',
            success: function (res) {
                if (res.confirm) {
                    let params = {
                        address_id: e.currentTarget.dataset.id
                    };
                    app.api.deleteAddress(params).then(function (res) {
                        if (res.code == 200) {
                            app.Toast.success('删除地址成功');
                            let addressList = [];
                            self.data.addressList.map(item => {
                                if (item.address_id != params.address_id) {
                                    addressList.push(item);
                                }
                            });
                            self.setData({
                                addressList: addressList
                            });
                        }
                    });
                }
            }
        });
    },
    setDefaultAddress: function (e) {
        let self = this;
        let address_id = e.currentTarget.dataset.id;
        var is_default = e.currentTarget.dataset.is_default;
        if (is_default == 1) {
            is_default = 2;
        } else {
            is_default = 1;
        }
        let params = {
            userId:app.globalData.userId,
            tag: 'default',
            address_id: address_id,
            is_default: is_default
        };
        app.api.editAddress(params).then(function (res) {
            if (res.code == 200) {
               // app.Toast.success('删除地址成功');
                self.getAddressList();
            }
        });
    },
    onLoad: function () {
        // console.log('onLoad')
    },
    onShow: function () {
        this.getAddressList();
    },
    getAddressList: function () {
        var self = this;
        let params = {
            userId: app.globalData.userId
        };
        app.api.getAddressList(params).then(function (res) {
            self.setData({
                addressList: res.content || []
            });
        });
    }
});
