var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    commonData: {

      tabList: [
        {
          type: "个人销售榜单",
          needIntegralNum: 600,
        },
        {
          type: "团队销售榜单",
          needIntegralNum: 700,
        },
        {
          type: "新增会员榜单",
          needIntegralNum: 1000,
        }
      ],
      rankingHeader: [
        "名次", "会员头像", "会员昵称", "会员编号", "环比"
      ],
      rankingList: [],
      myInfo: {
        "all_xs": "0",
        "sort": '',
        "avatar":"",
        "account_id": "",
        "all_user":0,
      },
      oIndex: 0
    }

  },
  changeTab(e) {
    var oIndex = e.currentTarget.dataset.index;
    var rankType = oIndex == 0 ? "rankPerson" : oIndex == 1 ? "rankTeam" :"rankRec";
    this.setData({
      commonData: {
        ...this.data.commonData,
        oIndex
      }
    });

    this.getRanking(rankType)

  },
  getRanking(rankType){
   let params = {
     userId: app.globalData.userId,
     page:1,
     limit:100
   }
    app.api.getTeamRank(rankType,params).then(res=>{
      var data = res.content;
       this.setData({
         commonData:{
           ...this.data.commonData,
           myInfo: data.myInfo,
           rankingList: data.rank_list
         }
       })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function() {
    this.getRanking('rankPerson')
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})