//index.js

//获取应用实例
var app = getApp();

Page({
    data: {
        userId: '',
        qrcodePath: ''
    },
    onLoad(options) {
        // console.log(options);
    },
    onShow() {
        this.getQrcode();
    },
    getQrcode: function(){
        let self = this;
        let params = {
            userId: app.globalData.userId
        };
        app.api.getMiniQrcode(params).then((res) => {
            self.setData({
                qrcodePath: res.content.qrcode_path || '' 
            });
        });
    },
    previewImage: function () {
        wx.previewImage({
            urls: this.data.qrcodePath.split(',')
            // 需要预览的图片http链接  使用split把字符串转数组。不然会报错
        })
    }
});
