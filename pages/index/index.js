//index.js
var QQMapWX = require('../../libs/js/qqmap-wx-jssdk.min.js');
var qqmapsdk;
//获取应用实例
var app = getApp();

Page({
    data: {
        level: '',
        account_code: '',
        banners: [],
        adverList: [0, 0, 0],
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        loadingHidden: false, // loading
        userId: '',
        userInfo_status: 1, //1：未获取；2：已获取
        userInfo: {},
        swiperCurrent: 0,
        categories: [],
        activeCategoryId: 0,
        homeGoodsData: [],
        scrollTop: 0,
        loadingMoreHidden: true,
        hasNoCoupons: true,
        coupons: [],
        searchInput: '',
        city: "", //城市信息  
        ldata: false,
         hasOpen:false,

    },
    onLoad(options) {
        // console.log('index-options = ',options);
        if (options.invite_code) {
            app.globalData.invite_code = options.invite_code;
            // console.log('index invite_code = ',app.globalData.invite_code );
        }
        if (options.scene) {
            app.globalData.invite_code = options.scene;
            // console.log('scene = ',app.globalData.invite_code );
        }
        this.initMap()
    },
    initMap() {
        qqmapsdk = new QQMapWX({
            key: 'IFWBZ-FXJL3-A7W36-YZ4BB-TAQYT-TWFJ3' //这里自己的key秘钥进行填充
        });
    },
    cancle(e) {
        this.setData({ldata: false})
    },
    ok(flag) {
      console.log('========' + flag.detail)
         console.log(flag)

        this.setData({ hasOpen: flag.detail })

        var that = this;
        if (!flag.detail) {
            that.setData({ldata: true});
        } else {
            that.setData({ldata: false});
            wx.getLocation({
              type: 'wgs84',
                success: function (res) {
                    var latitude = res.latitude;
                    var longitude = res.longitude;
                    that.getLocal(latitude, longitude);
                }
            })
        }
    },

    getLocation() {
        // 微信获得经纬度
        var that = this;
        wx.getLocation({
          type: 'wgs84',
            success: function (res) {
              console.log('授权了');
                var latitude = res.latitude;
                var longitude = res.longitude;
                that.getLocal(latitude, longitude);
                that.setData({
                    ldata: false
                })

              console.log('success====' + that.data.hasOpen); 
            },
            fail: function (res) {
                 console.log('未授权,弹出授权按钮====');              
                that.setData({
                    ldata: true
                })

              if (that.data.hasOpen){
                console.log('地理位置授权开关已打开???' + that.data.hasOpen);  
                that.setData({
                  ldata: false,
                  city:'定位失败'
                })
              }




            }
        })
    },
    // 获取当前地理位置
    getLocal: function (latitude, longitude) {
        let vm = this;
        qqmapsdk.reverseGeocoder({
            location: {
                latitude: latitude,
                longitude: longitude
            },
            success: function (res) {
                let city = res.result.ad_info.city.replace(/市/, '');
                // console.log(res.result);
                vm.setData({
                    city: city,
                    // ldata: false
                })

            },
            fail: function (res) {
                // console.log(res);
            },
            complete: function (res) {
                // console.log(res);
            }
        });
    },
    initLogin(o) {
        console.log(o.detail);
    },
    onShow() {
        // 获取城市定位
        this.getLocation();
        // console.log(app.globalData.level);
        // console.log(app.globalData.account_code);
        var self = this;
        wx.getStorage({
            key: 'level',
            success: function (res) {
                self.setData({
                    level: res.data
                });
            }
        });
        wx.getStorage({
            key: 'account_code',
            success: function (res) {
                self.setData({
                    account_code: res.data
                });
            }
        });
        self.getBarnerList();
        self.getGoodsTypes();
        self.getBanZjDatas();
        self.getHomeGoodsDatas();
    },
    getBarnerList() {
        app.api.getBannerList().then(res => {
            let banners = res.content;
            this.setData({banners});
        })
    },
    getGoodsTypes() {
        app.api.getGoodsTypes().then(res => {
            let categories = res.content;
            this.setData({categories});
        })
    },
    getBanZjDatas() {
        app.api.getBanZjDatas().then(res => {
            let adverList = res.content;
            this.setData({adverList});
        })
    },
    getHomeGoodsDatas() {
        app.api.getHomeGoodsDatas().then(res => {
            let homeGoodsData = res.content;
            this.setData({homeGoodsData});
        })
    },
    addToCart(e) {
        let id = e.currentTarget.dataset.id;
        let gspec_id = e.currentTarget.dataset.id;
        let params = {
            userId: app.globalData.userId,
            goods_id: id,
            gspec_id: gspec_id,
            goods_number: 1
        };
        app.api.addToCart(params).then(function () {
            app.Toast.success("加入购物车成功");
        })
    },
    categoryClick(e) {
        if (e.currentTarget.dataset.cate_id != 0) {
            wx.navigateTo({
                url: "/pages/goods-list/index?cate_id=" + e.currentTarget.dataset.cate_id
            })
        }
    },
    adverItemTap(e) {
        if (e.currentTarget.dataset.url) {
            wx.navigateTo({
                url: e.currentTarget.dataset.url
            })
        }
    },
    //事件处理函数
    swiperchange(e) {
        //console.log(e.detail.current)
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    orderDetailsTap(e) {
        wx.navigateTo({
            url: "/pages/goods-details/index?id=" + e.currentTarget.dataset.id
        })
    },
    tapBanner(e) {
        let url = e.currentTarget.dataset.url;
        if (url.indexOf('ishiyaji') > -1) {
            wx.navigateTo({
                url: '/pages/web-view/index?url=' + url
            })
        } else {
            wx.navigateTo({
                url: url
            })
        }
    },
    onPageScroll(e) {
        let scrollTop = this.data.scrollTop;
        this.setData({
            scrollTop: e.scrollTop
        })
    },
    listenerSearchInput(e) {
        this.setData({
            searchInput: e.detail.value
        })

    },
    toSearch() {
        let params = {
            key: this.data.searchInput
        };
        app.api.getGoodsList(params).then(res => {
            wx.setStorageSync('productList', res.content);
            wx.navigateTo({
                url: "/pages/goods-list/index"
            });
            // console.log(wx.getStorageSync('productList'));
            // console.log(typeof wx.getStorageSync('productList') );
        })
    },
    onShareAppMessage() {
        let level = this.data.level;
        let account_code = this.data.account_code;
        let url = '';
        if (level >= 1) {
            url = '/pages/index/index?invite_code=' + account_code;
        } else {
            url = '/pages/index/index';
        }
        // console.log('index_share_url = ',url);
        return {
            title: app.globalData.shareProfile,
            path: url,
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    }
});
