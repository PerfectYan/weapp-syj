var app = getApp();
Page({
    data: {
        statusType: ['待支付', '待发货', '待收货', '已签收', '申请退款', '申请退换货', '已取消', '已退款', '已退换货'],
        order_id: 0,
        goodsList: [],
        status: 0,
        order_sn: '',//订单编号
        count_num: '',// 商品图片
        addTime: '',// 下单时间
        total_fees: '',//商品总金额
        ship_fees: '',// 运费
        pay_fees: '',//支付金额
        pay_status: '',//支付状态   	1 = 未付款，2 = 已付款
        ship_status: '',// 配送状态   	1 = 未发货，2 = 未签收，3 = 已签收
        refund_status: '',// 售后状态   	0 = 未申请售后，1 = 申请退款，2 = 申请退换货
        cancel_status: '',// 订单关闭状态	1 = 未关闭，2 = 已关闭
        consignee: '',// 收货人
        phone: '',// 联系电话
        province: '',//省份
        city: '',//城市
        area: '',//区县
        address: '',// 详细地址
        mark: '',//备注

    },
    onLoad: function (e) {
        let self = this;
        let order_id = e.id;
        self.setData({order_id});

    },
    onShow: function () {
        let self = this;
        let order_id = this.data.order_id;
        app.api.getOrderDetails({order_id}).then(res => {
            let data = res.content;
            self.setData({
                status: data.status,
                order_sn: data.order_sn,//订单编号
                count_num: data.count_num,// 商品图片
                addTime: data.addTime,// 下单时间
                total_fees: data.total_fees,//商品总金额
                ship_fees: data.ship_fees,// 运费
                pay_fees: data.pay_fees,//支付金额
                pay_status: data.pay_status,//支付状态   	1 = 未付款，2 = 已付款
                ship_status: data.ship_status,// 配送状态   	1 = 未发货，2 = 未签收，3 = 已签收
                refund_status: data.refund_status,// 售后状态   	0 = 未申请售后，1 = 申请退款，2 = 申请退换货
                cancel_status: data.cancel_status,// 订单关闭状态	1 = 未关闭，2 = 已关闭
                consignee: data.consignee,// 收货人
                phone: data.phone,// 联系电话
                province: data.province,//省份
                city: data.city,//城市
                area: data.area,//区县
                address: data.address,// 详细地址
                mark: data.mark,//备注
                goodsList: data.goods
            })
        })
    },
    goGoodsDetail(e) {
        let goods_id = e.currentTarget.dataset.id;
        wx.navigateTo({
            url: '/pages/goods-details/index?id=' + goods_id,
        })
    },
    goUrl() {
        wx.navigateTo({
            url: '/pages/check-logistics/index?order_id=' + this.data.order_id,
        })
    },
    returnGoods(){
        let order_id = this.data.order_id;
        let type = 4;
        let params = {order_id, type};
        app.api.setOrderStatus(params).then(() => {
            app.Toast.success("您的申请已提交成功")
        })
    }
});