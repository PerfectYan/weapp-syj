//index.js

var app = getApp();

Page({
    data: {
        favorList: [],
        iconDelete: 'https://www.ishiyaji.com/static/images/my/icon-delete.png'
    },
    onShow: function () {
        this.getFavorList();
    },
    getFavorList: function () {
      let userId = app.globalData.userId;
      app.api.getCollectList({ userId}).then(res=>{
        this.setData({ favorList:res.content })
      })
    },
    deleteFavor: function (e) {
      let userId = app.globalData.userId;
      let index = e.currentTarget.dataset.index;
      let goods_id = e.currentTarget.dataset.id;
      let params = { userId, goods_id};
      app.api.goodsCollect(params).then( res=> {
        if (res.is_collect == 1){
          let favorList = this.data.favorList;
          favorList.splice(index,1);
          // console.log(favorList);
          this.setData({ favorList });
          app.Toast.success('取消收藏成功');
         }
      })
    },
    goGoodsDetail: function (e) {
        let id = e.currentTarget.dataset.id;
        // console.log('goGoodsDetail-id',e);
        wx.navigateTo({
            url: '/pages/goods-details/index?id='+id
        });
    }

});
