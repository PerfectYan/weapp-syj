//获取应用实例
var app = getApp();
Page({
    data: {
        id: '',
        commonCityData: {},
        addressData: {},
        provinces: [],
        citys: [],
        districts: [],
        selProvince: '请选择省',
        selCity: '请选择市',
        selDistrict: '请选择区/县',
        selProvinceIndex: 0,
        selCityIndex: 0,
        selDistrictIndex: 0,
        cityDisabled: true,
        districtDisabled: true
    },
    bindCancel: function () {
        wx.navigateBack({})
    },
    bindSave: function (e) {
        var consignee = e.detail.value.consignee;
        var detail_address = e.detail.value.detail_address;
        var phone = e.detail.value.phone;
        var zipcode = e.detail.value.zipcode;
        var commonCityData = this.data.commonCityData;

        if (consignee == "") {
            wx.showModal({
                title: '提示',
                content: '请填写联系人姓名',
                showCancel: false
            });
            return;
        }
        if (phone == "") {
            wx.showModal({
                title: '提示',
                content: '请填写手机号码',
                showCancel: false
            });
            return;
        }
        if (this.data.selProvince == "请选择省") {
            wx.showModal({
                title: '提示',
                content: '请选择地区',
                showCancel: false
            });
            return;
        }
        if (this.data.selCity == "请选择市") {
            wx.showModal({
                title: '提示',
                content: '请选择地区',
                showCancel: false
            });
            return;
        }
        var provinceId = commonCityData.cityData[this.data.selProvinceIndex].id;
        var cityId = commonCityData.cityData[this.data.selProvinceIndex].cityList[this.data.selCityIndex].id;
        var districtId;
        if (this.data.selDistrict == "请选择区/县" || !this.data.selDistrict) {
            districtId = '';
        } else {
            districtId = commonCityData.cityData[this.data.selProvinceIndex].cityList[this.data.selCityIndex].districtList[this.data.selDistrictIndex].id;
        }
        if (detail_address == "") {
            wx.showModal({
                title: '提示',
                content: '请填写详细地址',
                showCancel: false
            });
            return;
        }
        if (zipcode == "") {
            wx.showModal({
                title: '提示',
                content: '请填写邮编',
                showCancel: false
            });
            return;
        }
        var tag = '';
        if(this.data.id){
            tag = 'edit'
        }else{
            tag = 'add'
        }

        let params = {
            userId: app.globalData.userId,
            tag: tag,
            address_id: this.data.id,
            consignee: consignee,
            phone: phone,
            province: provinceId,
            city: cityId,
            area: districtId,
            detail_address: detail_address,
            zipcode: zipcode,
            is_default: 1 //不是默认地址
        };
        this.editAddress(params);
    },
    editAddress: function (params) {
        let self = this;
        app.api.editAddress(params).then(function () {
            let msg = params.tag == 'add' ? '新增地址成功' : '编辑地址成功';
            app.Toast.success(msg);
            if(self.data.from == 'order'){
                wx.navigateTo({
                    url: '/pages/select-address/index'
                });
            }else{
                wx.navigateBack({});
            }
        })
    },
    initCityData: function (level, obj) {
        var commonCityData = this.data.commonCityData;
        if (level == 1) {
            var pinkArray = [];
            for (var i = 0; i < commonCityData.cityData.length; i++) {
                pinkArray.push(commonCityData.cityData[i].name);
            }
            this.setData({
                provinces: pinkArray
            });
        } else if (level == 2) {
            var pinkArray = [];
            var dataArray = obj.cityList;
            for (var i = 0; i < dataArray.length; i++) {
                pinkArray.push(dataArray[i].name);
            }
            this.setData({
                citys: pinkArray
            });
        } else if (level == 3) {
            var pinkArray = [];
            var dataArray = obj.districtList;
            if(dataArray){
                for (var i = 0; i < dataArray.length; i++) {
                    pinkArray.push(dataArray[i].name);
                }
            }
            this.setData({
                districts: pinkArray
            });
        }
    },
    bindPickerCityTap: function(){
        // console.log(this.data.selProvince);
        if(this.data.selProvince == '请选择省'){
            wx.showToast({
                title: '请先选择省',
                icon: 'none',
                duration: 1500,
                mask: false
            });
        }else{
            this.setData({
                cityDisabled: false
            });
        }
    },
    bindPickerDistrictTap: function() {
        if(this.data.selProvince == '请选择省' || this.data.selCity == '请选择市'){
            wx.showToast({
                title: '请先选择省市',
                icon: 'none',
                duration: 1500,
                mask: false
            });
        }else{
            this.setData({
                districtDisabled: false
            });
        }
    },
    bindPickerProvinceChange: function (event) {
        var commonCityData = this.data.commonCityData;
        var selItem = commonCityData.cityData[event.detail.value];
        this.setData({
            selProvince: selItem.name,
            selProvinceIndex: event.detail.value,
            selCity: '请选择市',
            selCityIndex: 0,
            selDistrict: '请选择区/县',
            selDistrictIndex: 0
        });
        this.initCityData(2, selItem)
    },
    bindPickerCityChange: function (event) {
        var commonCityData = this.data.commonCityData;
        var selItem = commonCityData.cityData[this.data.selProvinceIndex].cityList[event.detail.value];
        this.setData({
            selCity: selItem.name,
            selCityIndex: event.detail.value,
            selDistrict: '请选择区/县',
            selDistrictIndex: 0
        });
        this.initCityData(3, selItem)
    },
    bindPickerDistrictChange: function (event) {
        var commonCityData = this.data.commonCityData;
        var selItem = commonCityData.cityData[this.data.selProvinceIndex].cityList[this.data.selCityIndex].districtList[event.detail.value];
        if (selItem && selItem.name && event.detail.value) {
            this.setData({
                selDistrict: selItem.name,
                selDistrictIndex: event.detail.value
            })
        }
    },
    onLoad: function (e) {
        let self = this;
        var commonCityData = app.globalData.commonCityData;
        self.setData({
            commonCityData: commonCityData
        });
        if(e.from && e.from == 'order'){
            self.setData({
                from: e.from
            });
        }else{
            self.setData({
                from: ''
            });
        }
        let id = e.id;
        if(id){
            self.setData({
                id: e.id
            });
        }
        self.initCityData(1);
        // console.log('address commonCityData = ',this.data.commonCityData);
    },
    onShow: function(){
        let self = this;
        let id = this.data.id;
        if (id) {
            wx.setNavigationBarTitle({
                title: '编辑地址'
            });
            setTimeout(function () {
                // 初始化原数据
                app.api.getAddressDetail({ address_id: id }).then(function (res) {
                    self.setData({
                        id: id,
                        addressData: res.content,
                        selProvince: res.content.province_address,
                        selCity: res.content.city_address,
                        selDistrict: res.content.area_address
                    });
                    self.setDBSaveAddressId(res.content);
                });
            },0);
        }
    },
    setDBSaveAddressId: function (data) {
        var commonCityData = this.data.commonCityData;
        var retSelIdx = 0;
        for (var i = 0; i < commonCityData.cityData.length; i++) {
            if (data.provinceId == commonCityData.cityData[i].id) {
                this.data.selProvinceIndex = i;
                for (var j = 0; j < commonCityData.cityData[i].cityList.length; j++) {
                    if (data.cityId == commonCityData.cityData[i].cityList[j].id) {
                        this.data.selCityIndex = j;
                        for (var k = 0; k < commonCityData.cityData[i].cityList[j].districtList.length; k++) {
                            if (data.districtId == commonCityData.cityData[i].cityList[j].districtList[k].id) {
                                this.data.selDistrictIndex = k;
                            }
                        }
                    }
                }
            }
        }
    },
    readFromWx: function () {
        let self = this;
        wx.chooseAddress({
            success: function (res) {
                let provinceName = res.provinceName;
                let cityName = res.cityName;
                let districtName = res.countyName;
                let retSelIdx = 0;
                var commonCityData = self.data.commonCityData;
                for (var i = 0; i < commonCityData.cityData.length; i++) {
                    if (provinceName == commonCityData.cityData[i].name) {
                        let eventJ = {detail: {value: i}};
                        self.bindPickerProvinceChange(eventJ);
                        self.data.selProvinceIndex = i;
                        for (var j = 0; j < commonCityData.cityData[i].cityList.length; j++) {
                            if (cityName == commonCityData.cityData[i].cityList[j].name) {
                                //that.data.selCityIndex = j;
                                eventJ = {detail: {value: j}};
                                self.bindPickerCityChange(eventJ);
                                for (var k = 0; k < commonCityData.cityData[i].cityList[j].districtList.length; k++) {
                                    if (districtName == commonCityData.cityData[i].cityList[j].districtList[k].name) {
                                        //that.data.selDistrictIndex = k;
                                        eventJ = {detail: {value: k}};
                                        self.bindPickerDistrictChange(eventJ);
                                    }
                                }
                            }
                        }
                    }
                }
                self.setData({
                    wxaddress: res
                });
            }
        })
    }
});
