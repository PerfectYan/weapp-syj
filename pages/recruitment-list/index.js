var app = getApp();

Page({
    /**
     * 页面的初始数据
     */
    data: {
        iconDate: 'https://www.ishiyaji.com/static/images/my/icon-date.png',
        tabList: [
            {type: "招募店长积分"},
            {type: "辅导店长积分"},
            {type: "管理店长积分"}
        ],
        oIndex: 0,
        type: 1,
        date: '',
        startDate: '2016-08',
        endDate: '',
        all_jf: '',
        recruitList: [],
        hidden: true,
    },
    changeTab(e) {
        let oIndex = e.currentTarget.dataset.index;
        let type = e.currentTarget.dataset.type;
        this.setData({oIndex, type});
        this.getRecruitList();
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        let type = options.type;
        let oIndex = type - 1;
        this.setData({oIndex, type});
    },
    bindDateChange: function (e) {
        // console.log('picker发送选择改变，携带值为', e.detail.value);
        this.setData({
            date: e.detail.value
        });
        this.getRecruitList();
    },
    getRecruitList: function () {
        let self = this;
        let params = {
            userId: app.globalData.userId,
            month: this.data.date,
            type: this.data.type,
            page: 1,
            limit: 10000
        };
        app.api.getZmjfList(params).then(res => {
            let content = res.content;
            self.setData({
                all_jf: content.all_jf,
                recruitList: content.list,
                hidden: content.list.length > 0 ? true : false
            })
        });
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let month = app.formatDate(new Date());
        this.setData({
            date: month,
            endDate: month
        });
        this.getRecruitList();
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
});