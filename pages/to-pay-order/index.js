//index.js
//获取应用实例
var app = getApp();

Page({
  data: {
    hasSetAddress: false,//是否填写了收获地址，默认false
    is_balance: 1,
    source_type: '0',
    cp_id: 0,// 优惠券id
    message: "",//留言
    address_id: "",
    consignee: "",
    phone: "",
    address: "",
    allPrice: 500,
    coupon_num: 0, //优惠券数量
    account_balance: 0,
    coupon_money: 0, // 优惠券的金额
    totalScoreToPay: 0,
    goodsList: [],

  },
  onShow: function () {
    let userId = app.globalData.userId;
    app.api.orderSettle({userId}).then(res => {
      this.init(res.content)
  })
  },
  onLoad: function (option) {
    // console.log('topayorder = ',option);
    var {coupon_money, cp_id, address_id} = option;
    if (coupon_money && cp_id) {
      this.setData({coupon_money, cp_id})
    }
  },
  init(data) {
    if (data) {
      this.setData({
        hasSetAddress: data.address.address ? true : false,
        address_id: data.address.address_id,
        consignee: data.address.consignee,
        phone: data.address.phone,
        address: data.address.address,
        allPrice: data.allPrice,
        coupon_num: data.coupon_num,
        account_balance: data.account_balance,
        goodsList: data.goods
      })
    }
  },
  addAddress: function () {
    wx.navigateTo({
      url: "/pages/address-add/index?from=order"
    })
  },
  selectAddress: function () {
    wx.navigateTo({
      url: "/pages/select-address/index"
    })
  },
  goCoupons(){
    wx.navigateTo({
      url: "/pages/my-coupons/index?price=" + this.data.allPrice
    })
  },
  setGoodsList: function (allPrice, goodsList) {
    this.setData({
      allPrice,
      goodsList
    });
  },
  totalPrice: function () {
    var list = this.data.goodsList;
    var total = 0;
    for (var i = 0; i < list.length; i++) {
      var curItem = list[i];
      total += parseFloat(curItem.shop_price) * parseInt(curItem.goods_number);
    }

    total = parseFloat(total.toFixed(2));//js浮点计算bug，取两位小数精度
    return total;
  },
  updateData(userId, up_type, list, address_id){
    var params = {userId, up_type};

    if (address_id !== undefined) {
      params["address_id"] = address_id;
    }
    else {
      var gspec = list.map(item => {
          return {
            "cart_id": item.cart_id,
            "gspec_id": item.gspec_id,
            "number": item.goods_number
          }
        });
      gspec = JSON.stringify(gspec);
      params["gspec"] = gspec;

    }

    app.api.orderUpdateData(params).then(res => {
      this.setGoodsList(this.totalPrice(), list);
    });
  },
  delItem: function (e) {
    var {index} = e.currentTarget.dataset;
    var userId = app.globalData.userId;
    var list = this.data.goodsList;
    list.splice(index, 1);

    this.updateData(userId, 1, list);
  },
  jiaBtnTap: function (e) {
    var that = this;
    var userId = app.globalData.userId;
    var {stock, index} = e.currentTarget.dataset;
    var list = that.data.goodsList;
    var nowNum = parseInt(list[parseInt(index)].goods_number);
    // 添加判断当前商品购买数量是否超过当前商品可购买库存

    if (nowNum < parseInt(stock)) {
      list[parseInt(index)].goods_number++;
      this.updateData(userId, 1, list);
    }

  },
  jianBtnTap: function (e) {
    var userId = app.globalData.userId;
    var {index} = e.currentTarget.dataset;
    var that = this;
    var list = that.data.goodsList;
    var nowNum = parseInt(list[parseInt(index)].goods_number);

    if (nowNum > 1) {
      list[parseInt(index)].goods_number--;
      this.updateData(userId, 1, list)
    }

  },
  inputHandel(event){
    this.setData({message: event.detail.value})
  },
    chooseBalance(){
    var is_balance = this.data.is_balance == 1 ? 2 : 1;
    this.setData({is_balance})
  },
  submitOrder(){
    let userId = app.globalData.userId;
    let invite_code = app.globalData.invite_code;
    let {address_id, message, cp_id, is_balance, goodsList} = this.data;
    // var cart_ids = this.data.goodsList.map(item => item.cart_id).toString();
    let gspec = goodsList.map(item => {
        return {
          "gspec_id": item.gspec_id,
          "number": item.goods_number
        }
      });

    gspec = JSON.stringify(gspec);

    if (!address_id) {
      app.Toast.warn("请填写收货地址");
      return;
    }

    let params = {
      userId,
      address_id,
      message,
      cp_id,
      is_balance,
      gspec,
      account_code: invite_code
    };

    // console.log('submitOrder = ',params);

    app.api.submitOrder(params).then(res => {
      if(res.need_pay == 1){
          let payParams = {
              orderSn: res.order_sn,
              url: "/pages/order-list/index?type=1"
          };
          app.api.wxPay(payParams);
      }else if(res.need_pay == 2){
        let params = {
            orderSn: res.order_sn
        };
        app.api.zeroPay(params).then(()=>{
            wx.switchTab({
                url: "/pages/order-list/index"
            })
        })
      }
    });
  }
});