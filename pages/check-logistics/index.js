// pages/check-logistics/index.js
var app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        express: '',
        express_no: '',
        order_id: '',
        arrivedList: []
    },
    copy() {
        let that = this;
        wx.setClipboardData({
            data: that.data.express_no,
            success: function (res) {
                // console.log(res.data)
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (option) {
        this.setData({
            order_id: option.order_id
        });

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let order_id = this.data.order_id;

        app.api.getOrderLogistics({order_id}).then(res => {
            let data = res.content;
            let arrivedList = data.data;
            arrivedList.map(item=>{
                item.date = item.time.substr(0,10);
                item.timeStr = item.time.substr(10);
            });
            // console.log(arrivedList);
            this.setData({
                express: data.express,
                express_no: data.express_no,
                arrivedList: arrivedList
            })
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})