//index.js
//获取应用实例
var app = getApp();
var WxParse = require('../../wxParse/wxParse.js');

Page({
    data: {
        level: '',
        account_code: '',
        autoplay: true,
        interval: 3000,
        duration: 1000,
        goodsId: '',
        goodsDetail: {},
        swiperCurrent: 0,
        hasMoreSelect: false,
        selectSize: "选择：",
        selectSizePrice: 0,
        totalScoreToPay: 0,
        shopNum: 0,
        hideShopPopup: true,
        spec_image: '',
        buyNum: 1,
        buyNumMin: 1,
        spec_vids: [],
        goodsSpecValue: {},
        canSubmit: false, //  选中规格尺寸时候是否允许加入购物车
        shopCarInfo: {},
        floorstatus: true,
        shopType: "addShopCart",//购物类型，加入购物车或立即购买，默认为加入购物车
        iconCollect: 'https://www.ishiyaji.com/static/images/icon-collect.png',
        userInfo:{}
    },

  /**
 * 生命周期函数--监听页面初次渲染完成
 */
  onReady: function () {
    //获得dialog组件
    this.dialog = this.selectComponent("#dialog");
  },
    //事件处理函数
    swiperchange: function (e) {
        //console.log(e.detail.current)
        this.setData({
            swiperCurrent: e.detail.current
        })
    },
    onLoad: function (options) {
        if (options.id) {
            this.setData({
                goodsId: options.id
            });
        }
        if(options.invite_code){
            app.globalData.invite_code = options.invite_code;
        }
    },
  initLogin(o) {
    let userId = o.detail;
    let self = this;

    wx.setStorageSync('_userId', userId);

    wx.getStorage({
      key: 'level',
      success: function (res) {
        self.setData({
          level: res.data
        });
      }
    });
    wx.getStorage({
      key: 'account_code',
      success: function (res) {
        self.setData({
          account_code: res.data
        });
      }
    });
    let params = {
      userId,
      goods_id: this.data.goodsId
    };
    app.api.getGoodsDetail(params).then(function (res) {
      let goodsDetail = res.content || {};
      WxParse.wxParse('goodsDesc', 'html', res.content.detail_desc, self, 5);
      var iconCollect = '';
      if (goodsDetail.is_collect == 2) {
        iconCollect = 'https://www.ishiyaji.com/static/images/icon-collected.png';
      } else {
        iconCollect = 'https://www.ishiyaji.com/static/images/icon-collect.png';
      }
      self.setData({
        iconCollect: iconCollect,
        goodsDetail: goodsDetail
      });
      if (self.data.goodsDetail.goods_tag != '1') {
        let param = {
          userId: app.globalData.userId
        };
        app.api.getPersonerInfo(param).then(res => {
          let data = res.content || {};
          self.setData({
            level: data.level,
            level_name: data.level_name
          });
          app.globalData.level_name = data.level_name;
        })
      }
    });
   
  },
  onShow() {
    let userId = wx.getStorageSync('_userId');
    userId && this.initLogin({ detail: userId })
  },
    setDefaultSpecValue: function (goodsDetail) {
        let self = this;
        let specList = goodsDetail.specList;
        let spec_vids = [];
        specList.map(spec => {
            spec.spec_value.map((value, index) => {
                if (index == 0) {
                    // 设置所有规格的第一个为默认
                    spec_vids.push(value.spec_vid);
                    value.active = true;
                } else {
                    value.active = false;
                }
            })
        });
        self.setData({
            goodsDetail: goodsDetail,
            spec_vids: spec_vids || []
        });
        self.getGoodsSpecValue();
        // console.log('spec_vids = ', self.data.spec_vids);
    },
    goShopCart: function () {
        wx.reLaunch({
            url: "/pages/shop-cart/index"
        });
    },
    addToShopCart: function () {
        this.setData({
            shopType: "addShopCart"
        });
        this.bindGuiGeTap();
    },
    tobuy: function () {
        this.setData({
            shopType: "tobuy"
        });
        var level = parseInt(this.data.level),
            goods_tag = this.data.goodsDetail.goods_tag,
            level_name = app.globalData.level_name;
        if(goods_tag> 1 && ((level+1) >= goods_tag)){
            wx.showToast({
                title: '您当前已经是'+level_name+'了',
                icon: 'none',
                duration: 2000,
                mask: false
            });
            return;
        }
        this.bindGuiGeTap();
        // goodsDetail

      if (goods_tag != '1'){
        let userId = app.globalData.userId;  
        app.api.getRecomenderInfo({ userId}).then(res=>{
          if (res.content.need_confirm == '2'){
            // 调组件内部方法显示弹窗
            this.dialog.show();
            this.dialog.setInfo(false, '确认推荐人', '您的推荐人是');
            this.dialog.setUserInfo(res.content);
            app.globalData.invite_code = res.content.account_code;
          }
        })

      }
    },
    goodsCollect: function () {
        let self = this;
        let params = {
            userId: app.globalData.userId,
            goods_id: self.data.goodsDetail.goods_id
        };
        app.api.goodsCollect(params).then(function (res) {
            var is_collect = '';
            var icon_collect = '';
                if(self.data.goodsDetail.is_collect == 1){
                    is_collect = 2;
                    icon_collect = 'https://www.ishiyaji.com/static/images/icon-collected.png';
                }else{
                    is_collect = 1;
                    icon_collect = 'https://www.ishiyaji.com/static/images/icon-collect.png';
                }
            let isCollect = {
                is_collect: is_collect
            };
            let newGoodsDetail = Object.assign(self.data.goodsDetail, isCollect);
            self.setData({
                iconCollect: icon_collect,
                goodsDetail: newGoodsDetail
            });
            app.Toast.success(res.message);
        })
    },
    /**
     * 规格选择弹出框
     */
    bindGuiGeTap: function () {
        this.setData({
            buyNum: 1,
            hideShopPopup: false
        });
        this.setDefaultSpecValue(this.data.goodsDetail);
    },
    /**
     * 规格选择弹出框隐藏
     */
    closePopupTap: function () {
        this.setData({
            hideShopPopup: true
        })
    },
    /**
     * 选择商品规格
     * @param {Object} e
     */
    labelItemTap: function (e) {
        let self = this;
        let spec_index = e.currentTarget.dataset.spec_index;
        let spec_value_index = e.currentTarget.dataset.spec_value_index;
        let specList = self.data.goodsDetail.specList;

        // 取消该分类下的子栏目所有的选中状态, 点击的规格
        let spec_values = specList[spec_index].spec_value;
        spec_values.map(item => {
            item.active = false;
        });

        // 设置当前选中状态
        spec_values[spec_value_index].active = true;
        // console.log('spec_values = ', spec_values);

        // console.log('specList = ', specList);
        // 获取所有的选中规格尺寸数据
        let needSelectNum = specList.length;
        let curSelectNum = 0;
        let spec_vids = [];
        for (let i = 0; i < specList.length; i++) {
            let values = specList[i].spec_value;
            for (let j = 0; j < values.length; j++) {
                if (values[j].active) {
                    curSelectNum++;
                    spec_vids.push(values[j].spec_vid)
                }
            }
        }

        let canSubmit = false;
        if (needSelectNum == curSelectNum) {
            canSubmit = true;
        }

        self.setData({
            goodsDetail: self.data.goodsDetail,
            spec_vids: spec_vids || [],
            canSubmit: canSubmit
        });

        // 计算当前价格
        if (canSubmit) {
            this.getGoodsSpecValue();
        }

    },
    getGoodsSpecValue: function () {
        let self = this;
        let params = {
            goods_id: self.data.goodsDetail.goods_id,
            spec_vids: self.data.spec_vids
        };
        app.api.getGoodsSpecValue(params).then(function (res) {
            // console.log(res);
            self.setData({
                goodsSpecValue: res.content || {},
                canSubmit: true
            });
        });
    },
    buyNumInput: function (e) {
        var buyNum = e.detail.value;
        var numReg = /^\+?[1-9][0-9]*$/;
        if (numReg.test(buyNum)) {
            if (parseInt(buyNum) > parseInt(this.data.goodsSpecValue.stock)) {
                buyNum = this.data.goodsSpecValue.stock
            } else if (parseInt(buyNum) < this.data.buyNumMin) {
                buyNum = this.data.buyNumMin;
            }
        } else {
            buyNum = 1;
        }
        this.setData({
            buyNum: buyNum
        });

    },
    numDecrementTap: function () {
        if (parseInt(this.data.buyNum) > this.data.buyNumMin) {
            let currentNum = this.data.buyNum;
            currentNum--;
            this.setData({
                buyNum: currentNum
            })
        }
    },
    numIncrementTap: function () {
        if (parseInt(this.data.buyNum) < parseInt(this.data.goodsSpecValue.stock)) {
            let currentNum = this.data.buyNum;
            currentNum++;
            this.setData({
                buyNum: currentNum
            })
        }
    },
    /**
     * 加入购物车
     */
    addShopCart: function () {
        let self = this;
        if (self.data.goodsDetail.goods_id && !this.data.canSubmit) {
            app.Toast.warn('请选择商品规格！');
            return;
        }
        if(self.data.goodsSpecValue.stock == 0){
            app.Toast.warn('您购买的商品库存不足！');
            return;
        }
        if (self.data.buyNum < 1) {
            app.Toast.warn('请输入正确的购买数量！');
            return;
        }

        let params = {
            userId: app.globalData.userId,
            goods_id: self.data.goodsDetail.goods_id,
            gspec_id: self.data.goodsSpecValue.gspec_id,
            goods_number: self.data.buyNum
        };

        app.api.addToCart(params).then(function () {
            self.closePopupTap();
            app.Toast.success('加入购物车成功');
        });
    },
    /**
     * 立即购买
     */
    buyNow: function () {
        let self = this;
        if (self.data.goodsDetail.specList && !self.data.canSubmit) {
            if (!self.data.canSubmit) {
                app.Toast.warn('请选择商品规格！');
            }
            return;
        }
        if(self.data.goodsSpecValue.stock == 0){
            app.Toast.warn('您购买的商品库存不足！');
            return;
        }
        if (self.data.buyNum < 1) {
            app.Toast.warn('请输入正确的购买数量！');
            return;
        }

        self.closePopupTap();

        let userId = app.globalData.userId;
        let data_type = 1;
        let gspec_id = self.data.goodsSpecValue.gspec_id;
        let goods_number = self.data.buyNum;

        let params = { userId, data_type, gspec_id, goods_number};

        app.api.orderSaveData(params).then( () =>{
            wx.navigateTo({
                url: '/pages/to-pay-order/index'
            });
        })
    },
    goHomePage: function () {
        wx.switchTab({
            url: "/pages/index/index"
        })
    },
    goTop(){
        if (wx.pageScrollTo) {
            wx.pageScrollTo({
                scrollTop: 0
            })
        } else {
            wx.showModal({
                title: '提示',
                content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
            })
        }
    },
    goCart() {
        wx.switchTab({
            url: '/pages/shop-cart/index',
        })
    },
    onShareAppMessage: function () {
        let level = this.data.level;
        let account_code = this.data.account_code;
        var url = '';
        if(level >=1){
            url = '/pages/goods-details/index?id=' + this.data.goodsDetail.goods_id + '&invite_code='+ account_code;
        }else{
            url = '/pages/goods-details/index?id=' + this.data.goodsDetail.goods_id;
        }
        // console.log('goods-details share url = ',url);
        return {
            title: this.data.goodsDetail.goods_name,
            path: url,
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    }
});
