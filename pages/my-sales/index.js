//index.js

var app = getApp();

Page({
    data: {
        type: '',
        statusType: ["今日销售额", "本月销售额", "总销售额"],
        currentType: 0,
        tabClass: ["", "", ""],
        salesName: '',
        salesValue: 6000,
        startDate: '2010-01-01',
        endDate: '',
        selectedDate: '2018-08-04',
        nowDate: '',
        salesList: []
    },
    onLoad: function (options) {
        var currentType = '';
        if(options.currentType){
            currentType = options.currentType;
        }
        let endDate = this.formatDate(new Date());
        let nowDate = new Date().getDate();
        this.setData({
            currentType: currentType,
            endDate: endDate,
            nowDate: nowDate,
        });
    },
    onShow: function () {

        this.getSalesList(1);
    },
    formatDate: function (date) {
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        return y + '-' + m + '-' + d;
    },
    getSalesList: function (saleType) {
      let userId = app.globalData.userId;
      let page = 1;
      let limit = 100;
      let type = saleType;
      let params = { userId, page, limit, type}
      app.api.mysale(params).then(res=>{
        let data = res.content;
        this.setData({
           all_money: data.all_money,
           salesList:data.list
         })
      })
    },
    statusTap: function (e) {
        var curType = e.currentTarget.dataset.index;
        this.data.currentType = curType;
        this.setData({
            currentType: curType
        });
      this.getSalesList(curType + 1);
    },
    datePickerChange: function (e) {
        let selectedDate = e.detail.value;
        let nowDate = new Date(selectedDate).getDate();
        this.setData({
            selectedDate: selectedDate,
            nowDate: nowDate
        })
    }

});
