// pages/sales-rank/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    rankingHeader: [
      "榜单", "会员", "直推新增", "自购", "销售"
    ],
    dateList: [],
    all_xs: 0,
    userinfo: {
      "avatar": "",
      "nickname": "",
      "level": "",
      "all_user": "0",
      "all_xs": "",
      "all_price": ""
    },
    rankingList: [],

    date: '',
    startDate: '2016-08',
    endDate: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.initDateList();
    this.getSalesList(this.data.dateList[0])

  },
  getSalesList(){
    let params = {
      userId: app.globalData.userId,
      month_start:  this.data.date,
      page: 1,
      limit: 100
    }
    app.api.getSalesList(params).then(res => {
      let data = res.content;
      this.setData({
        all_xs: data.all_xs,
        rankingList: data.list,
        userinfo: data.userinfo
      })
    })


  },
  bindDateChange(e){
    // console.log('picker发送选择改变，携带值为', e.detail.value);
    this.setData({
      date: e.detail.value
    });
    this.getSalesList()


  },
  format(n) {
    return n < 10 ? '0' + n  : n
  },
  initDateList() {
    var dateList = [];
    var year = new Date().getFullYear();
    var month = new Date().getMonth() + 1;
    for (var i = 1; i <= month; i++) {
      dateList.push(year + '-' + this.format(i))
    }
    this.setData({
      dateList
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    let month = app.formatDate(new Date());
    this.setData({
      date: month,
      endDate: month
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})