
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
      uid: undefined,
      amount_min: 1,
      points: 0,
      date: '',
      rechargePoint: '', //需要兑换的积分
      btnDisabled: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function() {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getZmjf();
    this.checkDrawDate();
  },
  getZmjf: function(){
    app.api.getZmjf({
      userId: app.globalData.userId
    }).then(res => {
      this.setData({
        points: res.content.account_balance
      })
    })
  },
  checkDrawDate: function(){
    app.api.checkDrawDate({
      userId: app.globalData.userId
    }).then(res => {
      this.setData({
        date: res.content.date
      })
    })
  },
  amountInput: function(e) {
    let amount = e.detail.value;
    this.setData({
      rechargePoint: amount.replace(/[^\d]/g, '')
    });
    if (amount == "" || parseFloat(amount) < this.data.amount_min || parseFloat(amount) > parseFloat(this.data.points)){
      this.setData({
        btnDisabled: true
      });
      return;
    }
    this.setData({
      btnDisabled: false
    });
  },
  bindRedeem: function() {
    let self = this;
    let userId = app.globalData.userId;
    let draw_money = this.data.rechargePoint;
    let points = this.data.points;
    let params = {
      userId,
      draw_money
    };
    let lastPoint = (points - draw_money).toFixed(2);
    app.api.getRechargeApply(params).then(() => {
      self.setData({
        points: lastPoint,
        rechargePoint: ''
      });
      app.Toast.success("兑换成功");
    })
  },
  goToRedeemList: function() {
    wx.navigateTo({
      url: '/pages/redeem-list/index'
    });
  }
});