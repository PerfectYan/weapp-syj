var app = getApp();
Page({
    data: {
        hidden: true,
        orderList: [],
        statusType: ["全部", "待支付", "待发货", "待收货", "退款/售后"],
        status: ['待支付', '待发货', '待收货', '已签收', '申请退款', '申请退换货', '已取消', '已退款', '已退换货'],
        currentType: 0,
        tabClass: ["", "", "", "", ""],
        score: 100,
    },
    statusTap: function (e) {
        let curType = e.currentTarget.dataset.index;
        this.setData({
            currentType: curType
        });
        // console.log('currentType = ', this.data.currentType);
        this.getOrderList(curType + 1);
    },
    payTap: function (e) {
        let orderSn = e.currentTarget.dataset.order_sn;
        let payParams = {
            orderSn: orderSn,
            url: "/pages/order-list/index?id=2"
        };
        app.api.wxPay(payParams);
    },
    cancelOrderTap: function (e) {
        let self = this;
        let orderId = e.currentTarget.dataset.id;
        let params = {
            order_id: orderId,
            type: 1
        };
        wx.showModal({
            title: '确定取消该订单吗？',
            content: '',
            success: function (res) {
                if (res.confirm) {
                    app.api.setOrderStatus(params).then(function () {
                        self.getOrderList(self.data.currentType + 1);
                    });
                }
            }
        });
    },
    setOrderStatus: function (e) {
        let self = this;
        let orderId = e.currentTarget.dataset.id;
        let type = e.currentTarget.dataset.type;
        let params = {
            order_id: orderId,
            type: type
        };
        let title = '';
        if(type == '2'){
            title = "确定申请退款吗？";
        }else if(type == '3'){
            title = "确定申请退换货吗？";
        }else if(type == '5'){
            title = "确定取消申请吗？";
        }
        if(type == '2' || type == '3' || type == '5'){
            wx.showModal({
                title: title,
                content: '',
                success: function (res) {
                    if (res.confirm) {
                        app.api.setOrderStatus(params).then(function () {
                            self.getOrderList(self.data.currentType + 1);
                        });
                    }
                }
            });
        }else{
            app.api.setOrderStatus(params).then(function () {
                self.getOrderList(self.data.currentType + 1);
            });
        }
    },
    logisticsDetailsTap: function (e) {
        let orderId = e.currentTarget.dataset.id;
        let currentType = this.data.currentType;
        let url = "/pages/check-logistics/index?order_id=" + orderId + "&currentType=" + currentType;
        wx.navigateTo({
            url: url
        })
    },
    orderDetailsTap: function (e) {
        wx.navigateTo({
            url: "/pages/order-details/index?id=" + e.currentTarget.dataset.id
        })
    },
    getOrderList(type) {
        let self = this;
        let userId = app.globalData.userId;
        let page = 1;
        let limit = 100;
        let params = {userId, type, page, limit};
        app.api.getOrderList(params).then(res => {
            let hidden = res.content.length ? true : false;
            self.setData({
                hidden,
                orderList: res.content
            });
            // console.log('type = ', type);
            // console.log('orderList = ', self.data.orderList);
        });
    },
    onLoad: function (options) {
        // 生命周期函数--监听页面显示
        // 获取订单列表
        if (options && options.type) {
            this.setData({
                currentType: options.type - 1
            });
        } else {
            this.setData({
                currentType: 0
            });
        }
    },
    onShow: function () {
        this.getOrderList(this.data.currentType + 1);
    },
    onHide: function () {
        // 生命周期函数--监听页面隐藏

    },
    onUnload: function () {
        // 生命周期函数--监听页面卸载

    },
    onPullDownRefresh: function () {
        // 页面相关事件处理函数--监听用户下拉动作

    },
    onReachBottom: function () {
        // 页面上拉触底事件的处理函数

    }
});
