var app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        iconScore: 'https://www.ishiyaji.com/static/images/my/icon-score.png',
        iconDate: 'https://www.ishiyaji.com/static/images/my/icon-date.png',
        date: '',
        startDate: '2016-08',
        endDate: '',
        salesData: {}
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function () {
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        let month = app.formatDate(new Date());
        this.setData({
            date: month,
            endDate: month
        });
        this.getSalesData(month);
    },
    bindDateChange: function (e) {
        // console.log('picker发送选择改变，携带值为', e.detail.value);
        this.setData({
            date: e.detail.value
        });
        this.getSalesData(this.data.date);
    },
    getSalesData: function (month) {
        let self = this;
        let params = {
            userId: app.globalData.userId,
            month: month
        };
        app.api.getXsjf(params).then(res => {
            let content = res.content;
            self.setData({
                salesData: content
            })
        });
    },
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    goRedeemTap: function () {
        wx.navigateTo({
            url: '/pages/redeem/index'
        });
    },
    goSalesList: function (e) {
        let type = e.currentTarget.dataset.type;
        wx.navigateTo({
            url: '/pages/sales-list/index?type=' + type
        });
    }
});