// pages/my-team/index.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    oIndex: -1,
    userId: '',
    searchInput: '',
    teamInfo: {
      all: 0,
      coachDz: 0, //---- - 辅导店长数
      adminDz: 0, //---- - 管理店长数
      jpzd: 0, //--------金牌店长数
      yxzj: 0, //--------营销总监
      syb: 0, //--------- 事业部总经理
      fgs: 0, //--------- 分公司董事长
    },
    shopList: []
  },
  changeTab(e) {
    var oIndex = e.currentTarget.dataset.index;
    if (oIndex == this.data.oIndex){
      this.setData({ oIndex:-1 })
       this.getShopList();
     }else{
      this.setData({  oIndex  })
      this.getShopList(parseInt(oIndex) + 1);
    }
  },
  onLoad: function(options) {
    let userId = '';
    if (options.id) {
      userId = options.id;
    } else {
      userId = app.globalData.userId;
    }
    this.setData({
      userId: userId
    });
  },
  onShow: function() {
    this.getMyTeamInfo();
    this.getShopList();
  },
  getMyTeamInfo() {
    let userId = this.data.userId;
    app.api.getMyTeamInfo({
      userId
    }).then(res => {
      var data = res.content;
      this.setData({
        teamInfo: data
      })
    })
  },
  search(keyword){
    let userId = this.data.userId;
    let page = 1;
    let limit = 100;
    let key = keyword;
    let params = {
      userId,
      page,
      limit,
      key
    };
    app.api.getShopList(params).then(res => {
      this.setData({
        oIndex:-1,
        shopList: res.content
      })
    })

  },
  getShopList(shop_type) {
    let userId = this.data.userId;
    let page = 1;
    let limit = 100;
    let params = {
      userId,
      page,
      limit
    };
    if (shop_type != undefined) {
      params["shop_type"] = shop_type
    }

    app.api.getShopList(params).then(res => {
      this.setData({
        shopList: res.content
      })
    })
  },
  listenerSearchInput: function(e) {
    this.setData({
      searchInput: e.detail.value
    })
  },
  toSearch: function() {
    // let reg = /[\u4e00-\u9fa5]+/;
    let keyword = this.data.searchInput;
    this.search(keyword)
    this.setData({
      searchInput: ''
    })
  },
  showZhitui(e){
    var userId = e.currentTarget.dataset.id;
    wx.redirectTo({
      url: '/pages/my-team/index?id=' + userId,
    })


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
});