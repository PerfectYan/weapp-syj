//index.js
//获取应用实例
var app = getApp();
Page({
  data: {
    back: '',
    couponList: []
  },
  onLoad: function(option) {
      var back = option.price > 0 ? "to-pay-order" : "index";
      this.setData({ back  });
      // console.log(option);
      if (option.price) {
         var params = {
           userId: app.globalData.userId,
           price: option.price
         };
         app.api.getCouponList(params).then(res => {
            this.setData({
              //  back,
               couponList: res.content
            });
         });
      } else {
         var params = {
             userId: app.globalData.userId,
         };
         app.api.getAllCouponList(params).then(res => {
            this.setData({
                couponList: res.content
            })
        });
      }
    },
    onShow: function () {

    },
    goBuy: function (e) {
        var {
            id,
            couponnum
        } = e.currentTarget.dataset;
        var query = "";
        if (this.data.back == "to-pay-order") {
            query = `?coupon_money=${couponnum}&cp_id=${id}`;
            wx.redirectTo({
                url: `/pages/${this.data.back}/index${query}`
            })
        } else {
            wx.switchTab({
                url: '/pages/index/index'
            })
        }
    }
});