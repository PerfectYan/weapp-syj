// components/dialog/dialog.js

var app = getApp();

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    content: {
      type: String
    },
    contentColor: {
      type: String,
      value: '#888888'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: true,
    hidden: false,
    title: "确认推荐人",
    recomendInfo:'您的推荐人是',
    inputVal:'',
    userInfo: {},
    userId: ''
  },
  /**
   * 组件的方法列表
   */
  methods: {
    setInfo(hidden, title, recomendInfo){
      this.setData({
        hidden,
        title ,
        recomendInfo,
      })
    },
    setUserInfo(userInfo){
      this.setData({
        userInfo
      })
    },
    hide() {
      this.setData({
        show: true
      })
    },
    show() {
      this.setData({
        show: false,
        inputVal:''
      })
    },
    bindKeyInput(e){
      this.setData({
        inputVal: e.detail.value
      })
    } ,   
    closeModal(e) {
      this.hide()
    },
    editConfirm(){
      if(this.data.inputVal){
        let userId = app.globalData.userId;
        let account_id = this.data.inputVal;
        app.api.editRecomenderInfo({ userId, account_id }).then(res => {
          if(res.code == 200){
            this.setData({ inputVal:''  });
            this.setInfo(true, '修改成功','您的推荐人已修改为');
            this.setUserInfo(res.content);
            app.globalData.invite_code = res.content.account_code;
          }else{
            app.Toast.alert('你输入ID不存在或该ID不是店长')
          }
         
        })
      }else{
        app.Toast.alert('请输入推荐人ID')
      }
    }

  },
  ready() {

  }


});