//app.js

const api = require('utils/api');
import {formatDate, Toast} from 'utils/util';


App({
    api: api,
    Toast: Toast,
    formatDate: formatDate,
    globalData: {
        level: '',
        level_name: '',
        userInfo: {},
        userId: '',
        userInfo_status: '',
        account_code: '',//我自己的用户编码，用于分享当参数带过去参数
        invite_code: '', //别人分享的带的别人的account_code，小程序内存为invite_code
        shareProfile: '诗雅集',
        commonCityData: {
            cityData: []
        }
    },
    onLaunch: function () {
        this.api.getCityData().then(res => {
            this.globalData.commonCityData.cityData = res;
            // console.log('app commonCityData', this.globalData.commonCityData);
        });
    }
});
